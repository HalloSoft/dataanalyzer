#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide2.QtWidgets import QItemDelegate, QTableWidget, QTableWidgetItem
from PySide2.QtCore import Qt

from FloatDelegate import FloatDelegate


class TableWidget(QTableWidget):

    def __init__(self):
        QTableWidget.__init__(self)

        self.setColumnCount(17)
        self.setRowCount(10)

        self.data = None

        self.verticalHeader().hide()
        self.setSortingEnabled(True)
        self.setItemDelegate(FloatDelegate(2, self))

    def set_data(self, data):
        self.data = data
        self.__update()

    def __update(self):
        if self.data:
            self.__set_dimensions()
            self.__set_headers()
            self.__set_values()
            self.__set_widths()

    def __set_dimensions(self):
        row_count = self.data.row_count()
        self.setRowCount(row_count)

        column_count = self.data.column_count()
        self.setColumnCount(column_count)

    def __set_headers(self):
        headers = self.data.headers()
        self.setHorizontalHeaderLabels(headers)
        self.__set_header_tooltips()

    def __set_header_tooltips(self):
        for i in range(0, self.columnCount()):
            item = self.horizontalHeaderItem(i)
            text = item.text()
            item.setToolTip(text)

    def __set_values(self):
        records = self.data.values()
        row_no = 0
        for record in records:
            self.__set_row_values(record, row_no)
            row_no += 1

    def __set_row_values(self, record, row):
        column_no = 0
        for value in record.values():
            item = QTableWidgetItem()
            item.setData(Qt.DisplayRole, value)
            self.setItem(row, column_no, item)
            column_no += 1

    def __set_widths(self):
        data_width = 60
        self.setColumnWidth(0, 150)
        for i in range(1, self.data.column_count()):
            self.setColumnWidth(i, data_width)
