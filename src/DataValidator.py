#!/usr/bin/python
# -*- coding: utf-8 -*-

class DataValidator:
    
    def __init__(self,  data, key_column_name=''):
        self.data = data
        self.isvalid = True
        self.key_column_name = key_column_name
        self.error_list = []
        
    def is_valid(self):
        # basic checks
        self.__check_basic()

        # comparison of packages
        if self.isvalid:
            self.__check_key_columns()

        # tests for each package
        for data_pack in self.data:
            self.__test_package(data_pack)
        return self.isvalid

    def errors(self):
        return self.error_list

    def __check_basic(self):
        if not self.key_column_name:
            self.isvalid = False
            self.error_list.append('No key column')

    def __test_package(self, pack):
        records = pack.values()
        last_len = records[0].length()
        for item in records:
            current_len = item.length()
            if current_len != last_len:
                self.isvalid = False
                self.error_list.append('Different length')

            self.__test_record(item)

    def __test_record(self, record):
        self.__test_valid_voters(record)

    def __test_valid_voters(self, record):
        voters_index = record.parent.column_index_by_name('Wahlberechtigte insgesamt')
        voters = record.value_by_index(voters_index)
        valid_voters_index = record.parent.column_index_by_name('Gültige Stimmen')
        valid_voters = record.value_by_index(valid_voters_index)
        if valid_voters > voters:
            name = record.parent.name
            name_index = record.parent.column_index_by_name('Gebiet-Name')
            record_name = record.value_by_index(name_index)

            self.error_list.append(f'{name}, {record_name}: More valid voters ({valid_voters}) than voters ({voters})')
            self.isvalid = False

    def __check_key_columns(self):
        keys = self.data[0].column_by_name(self.key_column_name)
        for data_pack in self.data:
            current_keys = data_pack.column_by_name(self.key_column_name)
            if keys != current_keys:
                self.error_list.append(f'Key-Columns \n {keys} - \n {current_keys}')
                self.isvalid = False
