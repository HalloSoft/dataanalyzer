#!/usr/bin/python
# -*- coding: utf-8 -*-

class DataModel:
    
    def __init__(self, name=''):
        self.header_list = []
        self.record_list = []
        self.key_name = None
        self.mapper = {}
        self.name = name
        
    def is_valid(self):
        result = False

        header_ok = len(self.header_list) > 0
        values_ok = len(self.record_list) > 0
        if header_ok and values_ok:
            result = True

        return result

    def row_count(self):
        return len(self.record_list)

    def column_count(self):
        return self.record_list[0].length()

    def headers(self):
        return self.header_list

    def key_column(self):
        return self.key_name

    def values(self):
        return self.record_list

    def value(self, model_index):
        row, col = model_index.coordinate()
        record = self.record_list[row]

        if not isinstance(record, DataRecord):
            raise ValueError

        result = record.value_by_index(col)
        return result

    def record_for_key(self, col_name, key):
        mapped_col_name = self.mapper[col_name]
        index = self.header_list.index(mapped_col_name)

        result = None
        for row in self.record_list:
            if row.value_by_index(index) == key:
                result = row
                break

        return result

    def column_index_by_name(self, name):
        if len(self.mapper) > 0:
            name = self.mapper[name]
        return self.header_list.index(name)

    def column_name_by_index(self, index):
        original_name = self.header_list[index]
        result = None
        for k, v in self.mapper.items():
            if v == original_name:
                result = k

        return result
        
    def column_by_name(self,  name):
        index = self.header_list.index(name)
        result = []
        for record in self.record_list:
            item = record.value_by_index(index)
            result.append(item)

        return result

    def set_name(self, name):
        self.name = name

    def set_key(self, key):
        self.key_name = key

    def insertValue(self, record):
        self.record_list.append(record)

    def appendHeader(self, header):
        self.header_list.append(header)

    def set_mapping_dictionary(self, dictionary):
        self.mapper = dictionary

    def print(self):
        self.__print_headers()
        for record in self.record_list:
            record.print()
        
    def __print_headers(self):
        for word in self.header_list:
            print(word,  end=' - ')
        
        print()
            

class DataRecord:

    def __init__(self, values, parent):
        self.data_values = values
        self.parent = parent

    def append(self, value):
        self.data_values.append(value)

    def values(self):
        return self.data_values
        
    def value_by_index(self,  index):
        return self.data_values[index]

    def length(self):
        return len(self.data_values)

    def parent(self):
        return self.parent
        
    def print(self):
        for item in self.data_values:
            print(item, end=' ')
            
        print()


class DataModelIndex:

    def __init__(self, owner, row=None, col=None):
        self.owner = owner
        self.row = row
        self.column = col
        self.is_valid = self.__is_row_col_valid()

    def coordinate(self):
        return self.row, self.column

    def owner(self):
        return self.owner

    def __is_row_col_valid(self):
        result = True

        if not isinstance(self.column, int):
            raise ValueError

        if not isinstance(self.row, int):
            raise ValueError

        if self.column is None:
            result = False

        if self.row is None:
            result = False

        return result
