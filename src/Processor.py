#!/usr/bin/python
# -*- coding: utf-8 -*-

from DataModel import DataRecord
from DataModel import DataModel


def format_values(record, index):
    current_value = record.value_by_index(index)

    return current_value


class Processor:

    def __init__(self):
        self.current_data = 0
        self.last_data = 0
        self.key_col = None
        self.static_columns = ["Gebiet-Name"]
        self.compare_columns = ["Wahlberechtigte insgesamt", "Gültige Stimmen"]
        self.party_columns = ["Die Linke", "Grüne", "CDU", "AfD"]
        self.calculated_columns = ["Prozent"]
        self.result_data = DataModel()

    def set_current_data(self, data):
        self.current_data = data
        self.key_col = data.key_column()

    def set_previous_data(self, data):
        self.last_data = data

    def result(self):
        if not self.__is_valid():
            raise Exception('Processor is not valid')
        self.__calculate()

        return self.result_data

    def __calculate(self):
        self.__init_headers()
        for record in self.current_data.values():
            record_previous_data = self.__get_record_old_data(record)
            row = self.__create_result_record(record, record_previous_data, self.result_data)
            self.result_data.insertValue(row)

    def __is_valid(self):
        previous_ok = self.last_data.is_valid()
        current_ok = self.current_data.is_valid()
        return previous_ok and current_ok

    def __init_headers(self):
        for column in self.static_columns:
            self.result_data.appendHeader(column)

        for column in self.compare_columns:
            self.result_data.appendHeader(column)
            self.result_data.appendHeader(f'{column} (old)')
            self.result_data.appendHeader(f'{column} (diff)')

        for column in self.party_columns:
            self.result_data.appendHeader(column)
            self.result_data.appendHeader(f'{column} (old)')
            self.result_data.appendHeader(f'{column} (diff)')

            for ccolumn in self.calculated_columns:
                self.result_data.appendHeader(ccolumn)
                self.result_data.appendHeader(f'{ccolumn} (old)')
                self.result_data.appendHeader(f'{ccolumn} (diff)')

    def __create_result_record(self, record, record_previous, parent):
        result = DataRecord([], parent)

        # static columns
        for party in self.static_columns:
            index = self.current_data.column_index_by_name(party)
            value = format_values(record, index)
            result.append(value)

        # compare values
        for party in self.compare_columns:
            index_current = self.current_data.column_index_by_name(party)
            value_current = format_values(record, index_current)
            result.append(value_current)

            index_previous = self.last_data.column_index_by_name(party)
            value_previous = format_values(record_previous, index_previous)
            result.append(value_previous)

            difference = value_current - value_previous
            result.append(difference)

        # parties
        for party in self.party_columns:
            index_current = self.current_data.column_index_by_name(party)
            value_current = format_values(record, index_current)
            result.append(value_current)

            index_previous = self.last_data.column_index_by_name(party)
            value_previous = format_values(record_previous, index_previous)
            result.append(value_previous)

            difference = value_current - value_previous
            result.append(difference)

            # calculated
            for calculated_column_name in self.calculated_columns:
                value = self.__calculate_column(calculated_column_name, record, self.current_data, party)
                result.append(value)
                value_previous = self.__calculate_column(calculated_column_name, record_previous, self.last_data, party)
                result.append(value_previous)
                difference = value - value_previous
                result.append(difference)

        return result

    def __calculate_column(self, name, record, data_base, party):
        value = None
        if name == 'Prozent':
            value = self.__calculate_percentage(record, data_base, party)

        return value

    def __calculate_percentage(self, record, data_base, party_name):
        base_value_index = data_base.column_index_by_name('Gültige Stimmen')
        base_value = record.value_by_index(base_value_index)
        percentage_value_index = data_base.column_index_by_name(party_name)
        percentage_value = record.value_by_index(percentage_value_index)
        value = 100.0 * float(percentage_value) / float(base_value)
        return value

    def __get_record_old_data(self, record):
        index = self.current_data.column_index_by_name(self.key_col)
        key = record.value_by_index(index)
        result = self.last_data.record_for_key(self.key_col, key)
        return result
