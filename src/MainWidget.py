#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide2.QtCore import Slot
from PySide2.QtWidgets import QFileDialog, QWidget
from Ui_MainWidget import Ui_MainWidget

from DataValidator import DataValidator
from KeysReader import KeysReader
from Processor import Processor
from Reader import Reader
from TableWidget import TableWidget

path = '../Data'


class MainWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        # Attributes
        self.ui = Ui_MainWidget()

        # init
        self.ui.setupUi(self)
        self.__initUi()
        self.ui.editPathCurrent.setText('../Data/2021-Land-BW-Stadtteile.csv')
        self.ui.editPathPrevios.setText('../Data/2016-Land-BW-Stadtteile.csv')

        # connections
        self.ui.buttonChooseCurrent.clicked.connect(self.__choose_current_file)
        self.ui.buttonChoosePrevious.clicked.connect(self.__choose_previous_file)
        self.ui.buttonGo.clicked.connect(self.__go)

    @Slot()
    def __choose_current_file(self):
        file_path, _ = QFileDialog.getOpenFileName(self, 'Current file', path)
        if file_path:
            self.ui.editPathCurrent.setText(file_path)

    @Slot()
    def __choose_previous_file(self):
        file_path, _ = QFileDialog.getOpenFileName(self, 'Previopus file', path)
        if file_path:
            self.ui.editPathPrevios.setText(file_path)

    @Slot()
    def __go(self):
        data_pack = self.__read_data()
        is_data_valid = self.__validate(data_pack)
        if is_data_valid:
            proc = Processor()
            proc.set_current_data(data_pack[0])
            proc.set_previous_data(data_pack[1])
            result_data = proc.result()
            self.table_widget.set_data(result_data)

    def __read_data(self):
        current_data = self.__create_data_file(self.ui.editPathCurrent.text())
        current_data.set_name('Stadtteile 2021')
        current_data.set_key('Gebiet-Name')
        keys_current = KeysReader('../Data/Keys.csv', 'Name 2021')
        current_data.set_mapping_dictionary(keys_current.dictionary())
        previous_data = self.__create_data_file(self.ui.editPathPrevios.text())
        previous_data.set_name('Stadtteile 2016')
        previous_data.set_key('Gebiet-Name')
        keys_previous = KeysReader('../Data/Keys.csv', 'Name 2016')
        previous_data.set_mapping_dictionary(keys_previous.dictionary())
        return current_data, previous_data

    def __validate(self, data):
        validator = DataValidator(data, 'gebiet-name')
        return validator.is_valid()

    def __create_data_file(self, data_path):
        reader = Reader(data_path)
        reader.read()
        return reader.result()

    def __initUi(self):
        self.table_widget = TableWidget()

        layout = self.layout()
        layout.addWidget(self.table_widget)
