#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide2.QtWidgets import QItemDelegate
from PySide2.QtCore import Qt


class FloatDelegate(QItemDelegate):
    def __init__(self, decimals, parent=None):
        QItemDelegate.__init__(self, parent=parent)
        self.nDecimals = decimals

    def paint(self, painter, option, index):
        value = index.model().data(index, Qt.EditRole)
        if isinstance(value, float):
            number = float(value)
            painter.drawText(option.rect, Qt.AlignLeft, "{:.{}f}".format(number, self.nDecimals))
        else:
            QItemDelegate.paint(self, painter, option, index)
