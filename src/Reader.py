#!/usr/bin/python
# -*- coding: utf-8 -*-

from DataModel import DataModel
from DataModel import DataRecord
import codecs


def trim(line):
    return line.strip()


def convert(value):
    result = None
    try:
        result = int(value)
    except ValueError:
        try:
            result = float(value)
        except ValueError:
            result = value

    return result


class Reader:
    
    def __init__(self,  filepath):
        self.filepath = filepath
        self.data = DataModel()
        self.delimiter = ';'
        
    def read(self):
        with codecs.open(self.filepath, encoding='utf-8', mode='r') as reader:
            linecounter = 0
            for line in reader.readlines():
                if linecounter == 0:
                    self.__interpret(line,  True)
                else:
                    self.__interpret(line,  False)
                    
                linecounter += 1
                
    def result(self):
        return self.data
        
    def __interpret(self, line, is_headline):
        line = trim(line)
        if line and not is_headline:
            self.__add_values(line)
        if line and is_headline:
            self.__add_headers(line)

    def __add_headers(self,  line):
        tokens = line.split(self.delimiter)
        for word in tokens:
            self.data.appendHeader(word)
            
    def __add_values(self,  line):
        values = self.__prepare_value_line(line)
        item = DataRecord(values, self.data)
        self.data.insertValue(item)

    def __prepare_value_line(self,  line):
        parts = line.split(self.delimiter)

        result = []
        for part in parts:
            value = convert(part)
            result.append(value)

        return result

