#!/usr/bin/python
# -*- coding: utf-8 -*-

from DataValidator import DataValidator
from KeysReader import KeysReader
from MainWidget import MainWidget
from Processor import Processor
from Reader import Reader

from PySide2.QtWidgets import QApplication

import sys

if __name__ == "__main__":
    app = QApplication(sys.argv)

    reader1 = Reader('../Data/2021-Land-BW-Stadtteile.csv')
    # reader1 = Reader('../Data/2021-Gemeinden.csv')
    reader1.read()
    current_data = reader1.result()
    current_data.set_name('Stadtteile 2021')
    keys_current = KeysReader('../Data/Keys.csv', 'Name 2021')
    current_data.set_mapping_dictionary(keys_current.dictionary())
    current_data.set_key('Gebiet-Name')

    reader2 = Reader('../Data/2016-Land-BW-Stadtteile.csv')
    # reader2 = Reader('../Data/2016-Gemeinden.csv')
    reader2.read()
    previous_data = reader2.result()
    previous_data.set_name('Stadtteile 2016')
    keys_previous = KeysReader('../Data/Keys.csv', 'Name 2016')
    previous_data.set_mapping_dictionary(keys_previous.dictionary())
    previous_data.set_key('Gebiet-Name')

    data_packs = [current_data, previous_data]
    validator = DataValidator(data_packs, 'gebiet-name')

    if validator.is_valid():
        print('Data valid')

        proc = Processor()
        proc.set_current_data(current_data)
        proc.set_previous_data(previous_data)
        result_data = proc.result()
        result_data.print()

        w = MainWidget()
        w.show()
        sys.exit(app.exec_())
    else:
        print('Data not valid')
        for error in validator.errors():
            print(error)
