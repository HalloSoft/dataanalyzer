#!/usr/bin/python
# -*- coding: utf-8 -*-

import codecs


class KeysReader:

    def __init__(self,  filepath, data_column):
        # initialisation of
        self.dict = {}
        self.data_column_index = 0
        self.data_column_name = data_column

        self.__read(filepath)

    def dictionary(self):
        return self.dict

    def print(self):
        for key in self.dict:
            print(f'{key} -> {self.dict[key]}')

    def __read(self, filepath):
        with codecs.open(filepath, encoding='utf-8', mode='r') as reader:
            counter = 0
            for line in reader.readlines():
                line = line.strip()
                if counter == 0:
                    self.__set_column_index(line)
                else:
                    self.__add_data(line)

                counter += 1

    def __set_column_index(self, line):
        tokens = line.split(';')
        self.data_column_index = tokens.index(self.data_column_name)

    def __add_data(self, line):
        tokens = line.split(';')
        key = tokens[self.data_column_index]
        value = tokens[0]
        self.dict[key] = value
