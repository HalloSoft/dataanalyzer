#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as mplt


def percentage_row(row,  baserow):
    result = []
    for val,  base in zip(row,  baserow):
        percent = float(val) / float(base)
        result.append(percent)
        
    return result
        
        
def show(data):
    row0 = data.column_by_name('gültig')
    row1 = data.column_by_name('Linke')
    row2 = data.column_by_name('CDU')
    row3 = data.column_by_name('SPD')

    x_axis = data.column_by_name('Bez') 
    perc1 = percentage_row(row1,  row0)
    perc2 = percentage_row(row2,  row0)
    perc3 = percentage_row(row3,  row0)
    
    np_array = np.array([perc1,  perc2,  perc3])
    
    np_array = np.transpose(np_array)
    print(np_array)
    
    mplt.plot(x_axis,  np_array,  's-')
    mplt.show()
